<?php
/**
 * @file
 * feds_main_nav.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feds_main_nav_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_context_default_contexts_alter().
 */
function feds_main_nav_context_default_contexts_alter(&$data) {
  if (isset($data['main_menu'])) {
    $data['main_menu']->reactions['block']['blocks']['system-main-menu']['region'] = 'global_header'; /* WAS: 'sidebar_first' */
    $data['main_menu']->reactions['block']['blocks']['system-main-menu']['weight'] = -8; /* WAS: -10 */
  }
}

/**
 * Implements hook_field_default_fields_alter().
 */
function feds_main_nav_field_default_fields_alter(&$data) {
  if (isset($data['node-schemaorg_event-field_schemaorg_date'])) {
    $data['node-schemaorg_event-field_schemaorg_date']['field_instance']['display']['default']['settings']['show_remaining_days'] = FALSE; /* WAS: '' */
    $data['node-schemaorg_event-field_schemaorg_date']['field_instance']['display']['teaser']['settings']['show_remaining_days'] = FALSE; /* WAS: '' */
  }
}

/**
 * Implements hook_user_default_permissions_alter().
 */
function feds_main_nav_user_default_permissions_alter(&$data) {
  if (isset($data['administer search'])) {
    $data['administer search']['roles']['anonymous user'] = 'anonymous user'; /* WAS: '' */
    $data['administer search']['roles']['authenticated user'] = 'authenticated user'; /* WAS: '' */
  }
  if (isset($data['search content'])) {
    $data['search content']['roles']['anonymous user'] = 'anonymous user'; /* WAS: '' */
    $data['search content']['roles']['authenticated user'] = 'authenticated user'; /* WAS: '' */
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function feds_main_nav_strongarm_alter(&$data) {
  if (isset($data['responsive_menu_combined_settings'])) {
    $data['responsive_menu_combined_settings']->value['main-menu']['friendly_name'] = 'WUSA'; /* WAS: 'THIS SITE' */
  }
}
