<?php
/**
 * @file
 * feds_main_nav.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function feds_main_nav_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: context
  $overrides["context.main_menu.reactions|block|blocks|system-main-menu|region"] = 'global_header';
  $overrides["context.main_menu.reactions|block|blocks|system-main-menu|weight"] = -8;

  // Exported overrides for: field
  $overrides["field.node-schemaorg_event-field_schemaorg_date.field_instance|display|default|settings|show_remaining_days"] = FALSE;
  $overrides["field.node-schemaorg_event-field_schemaorg_date.field_instance|display|teaser|settings|show_remaining_days"] = FALSE;

  // Exported overrides for: user_permission
  $overrides["user_permission.administer search.roles|anonymous user"] = 'anonymous user';
  $overrides["user_permission.administer search.roles|authenticated user"] = 'authenticated user';
  $overrides["user_permission.search content.roles|anonymous user"] = 'anonymous user';
  $overrides["user_permission.search content.roles|authenticated user"] = 'authenticated user';

  // Exported overrides for: variable
  $overrides["variable.responsive_menu_combined_settings.value|main-menu|friendly_name"] = 'WUSA';

 return $overrides;
}
