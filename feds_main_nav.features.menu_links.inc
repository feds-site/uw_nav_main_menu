<?php
/**
 * @file
 * feds_main_nav.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function feds_main_nav_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_about:node/22.
  $menu_links['main-menu_about:node/22'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/22',
    'router_path' => 'node/%',
    'link_title' => 'About',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_about:node/22',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -41,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_advocacy:node/154.
  $menu_links['main-menu_advocacy:node/154'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/154',
    'router_path' => 'node/%',
    'link_title' => 'Advocacy',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_advocacy:node/154',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -37,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_blog:blog.
  $menu_links['main-menu_blog:blog'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'blog',
    'router_path' => 'blog',
    'link_title' => 'Blog',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_blog:blog',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_clubs--services:node/84.
  $menu_links['main-menu_clubs--services:node/84'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/84',
    'router_path' => 'node/%',
    'link_title' => 'Clubs & Services',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_clubs--services:node/84',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -36,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_clubs:node/137.
  $menu_links['main-menu_clubs:node/137'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/137',
    'router_path' => 'node/%',
    'link_title' => 'Clubs',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_clubs:node/137',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_contact:node/38.
  $menu_links['main-menu_contact:node/38'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/38',
    'router_path' => 'node/%',
    'link_title' => 'Contact',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_contact:node/38',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -30,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_events:event.
  $menu_links['main-menu_events:event'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'event',
    'router_path' => 'event',
    'link_title' => 'Events',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_events:event',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -35,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_events:events.
  $menu_links['main-menu_events:events'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'events',
    'router_path' => 'events',
    'link_title' => 'Events',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_events:events',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_feds-club-listing:clubs/listing.
  $menu_links['main-menu_feds-club-listing:clubs/listing'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'clubs/listing',
    'router_path' => 'clubs/listing',
    'link_title' => 'Feds Club Listing',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_feds-club-listing:clubs/listing',
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -27,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_feds-services:feds-services.
  $menu_links['main-menu_feds-services:feds-services'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'feds-services',
    'router_path' => 'feds-services',
    'link_title' => 'Feds Services',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_feds-services:feds-services',
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -28,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_get-involved:node/151.
  $menu_links['main-menu_get-involved:node/151'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/151',
    'router_path' => 'node/%',
    'link_title' => 'Get Involved',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_get-involved:node/151',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -34,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_governance:node/70.
  $menu_links['main-menu_governance:node/70'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/70',
    'router_path' => 'node/%',
    'link_title' => 'Governance',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_governance:node/70',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -38,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_jobs--volunteer:opportunities.
  $menu_links['main-menu_jobs--volunteer:opportunities'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'opportunities',
    'router_path' => 'opportunities',
    'link_title' => 'Jobs & Volunteer',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_jobs--volunteer:opportunities',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -32,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_news:news.
  $menu_links['main-menu_news:news'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'news',
    'router_path' => 'news',
    'link_title' => 'News',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_news:news',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_our-people:about/people.
  $menu_links['main-menu_our-people:about/people'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'about/people',
    'router_path' => 'about/people',
    'link_title' => 'Our people',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_our-people:about/people',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_people-profiles:people-profiles.
  $menu_links['main-menu_people-profiles:people-profiles'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'people-profiles',
    'router_path' => 'people-profiles',
    'link_title' => 'People profiles',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_people-profiles:people-profiles',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -42,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_projects:projects.
  $menu_links['main-menu_projects:projects'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'projects',
    'router_path' => 'projects',
    'link_title' => 'Projects',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_projects:projects',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_services:services.
  $menu_links['main-menu_services:services'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'services',
    'router_path' => 'services',
    'link_title' => 'Services',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_services:services',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_staff-directory:feds_contact/staff-directory.
  $menu_links['main-menu_staff-directory:feds_contact/staff-directory'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'feds_contact/staff-directory',
    'router_path' => 'feds_contact/staff-directory',
    'link_title' => 'Staff Directory',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_staff-directory:feds_contact/staff-directory',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -29,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_your-money:node/152.
  $menu_links['main-menu_your-money:node/152'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/152',
    'router_path' => 'node/%',
    'link_title' => 'Your Money',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'main-menu_your-money:node/152',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -31,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('About');
  t('Advocacy');
  t('Blog');
  t('Clubs');
  t('Clubs & Services');
  t('Contact');
  t('Events');
  t('Feds Club Listing');
  t('Feds Services');
  t('Get Involved');
  t('Governance');
  t('Jobs & Volunteer');
  t('News');
  t('Our people');
  t('People profiles');
  t('Projects');
  t('Services');
  t('Staff Directory');
  t('Your Money');

  return $menu_links;
}
